#!/bin/bash
# Part 1: 基本配置設定
ODOO_VERSION="16.0"
POSTGRES_VERSION="13"
ODOO_PORT="8069"  # 可以根據需要更改端口號

# 配置目錄（相對於當前腳本的路徑）
CONFIG_DIR="./config"
LOGS_DIR="./logs"
ADDONS_DIR="./addons"

# Part 2: 創建 Odoo 的配置文件並設定日誌
# 創建配置、日誌和附加模塊目錄
mkdir -p ${CONFIG_DIR}
mkdir -p ${LOGS_DIR}
mkdir -p ${ADDONS_DIR}


# 檢查 Docker 是否安裝
if ! [ -x "$(command -v docker)" ]; then
  echo '錯誤：Docker 未安裝。' >&2
  exit 1
fi

# 檢查 Docker Compose 是否安裝
if ! [ -x "$(command -v docker-compose)" ]; then
  echo '錯誤：Docker Compose 未安裝。' >&2
  exit 1
fi

# 創建 Odoo 配置文件
cat <<EOF >${CONFIG_DIR}/odoo.conf
[options]
addons_path = /mnt/extra-addons
logfile = /var/log/odoo/odoo.log
EOF

# Part 3: 設定 docker-compose.yml
# 創建 docker-compose.yml 文件
cat <<EOF >docker-compose.yml
version: '3.7'
services:
  db:
    image: postgres:${POSTGRES_VERSION}
    volumes:
      - odoo-db:/var/lib/postgresql/data
    environment:
      POSTGRES_DB: postgres
      POSTGRES_USER: odoo
      POSTGRES_PASSWORD: odoo
    restart: unless-stopped

  odoo:
    image: odoo:${ODOO_VERSION}
    depends_on:
      - db
    ports:
      - "${ODOO_PORT}:8069"
    volumes:
      - ${ADDONS_DIR}:/mnt/extra-addons
      - ${LOGS_DIR}:/var/log/odoo
      - ${CONFIG_DIR}/odoo.conf:/etc/odoo/odoo.conf
    restart: unless-stopped

volumes:
  odoo-db:
EOF

# 啟動服務
docker-compose up -d

echo "Odoo ${ODOO_VERSION} 和 PostgreSQL ${POSTGRES_VERSION} 安裝並啟動完成。"

